from django.urls import path
from . import views

# Adding a namespace to this urls.py helps django distinguish this set of routes from other urls.py files in other packages
app_name = 'todolist'
urlpatterns = [
	path('', views.index, name = "index"),
	path('<int:todoitem_id>/', views.todoitem, name="viewtodoitem"),
	path('register/', views.register, name = "register"),
	path('update_profile/', views.update_profile, name = "update_profile"),
	path('login/', views.login_view, name="login"),
	path('logout/', views.logout_view, name="logout"),
	path('add_task/', views.add_task, name="add_task"),
	path('<int:todoitem_id>/edit', views.update_task, name="update_task"),
	path('<int:todoitem_id>/delete', views.delete_task, name="delete_task"),
	path('event/<int:eventitem_id>/', views.eventitem, name="vieweventitem"),
	path('add_event/', views.add_event, name="add_event"),
	path('event/<int:eventitem_id>/delete', views.delete_event, name="delete_event"),
]

# django design pattern MVT - Model, View, Template